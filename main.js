// 1. setTimeout() - это функция, которая выполнит код один раз , через определенный промежуток времени, который мы укажем.
// setInterval()  - это функция, которая будет выполнять код каждый раз через определенный промежуток времени.
// 2. в функцию можно передать нулевую задержку, но эта функция будет выполнена только после того, как весь код обработается, по этому нет гарантий, что результат будет такой как нам нужен.
// 3.Если не удалять таймеры, то на сайте могут происходить утечки памяти и баги из-за неожиданных срабатываний функции.
let body = document.body;
let images = document.querySelector(".images-wrapper").children;
let numberOfImage = 0;
let isRunning = true;
let delay = 0;

let timerId = setInterval(showImage, delay);

function showImage() {
  Array.from(images).map((item) => (item.style.display = "none"));
  images[numberOfImage].style.display = "block";
  numberOfImage++;
  if (numberOfImage === 4) {
    numberOfImage = 0;
  }
  delay = 1000;
  clearInterval(timerId);
  timerId = setInterval(showImage, delay);
}

function createButton(name, color) {
  let button = document.createElement("button");
  button.textContent = name;
  button.style.backgroundColor = color;
  button.style.borderRadius = "4px";
  button.style.margin = "20px";
  button.style.padding = "20px";
  return button;
}
let stopButton = createButton("Остановить", "red");
let continueButton = createButton("Продолжить", "green");

function addButtons() {
  body.append(stopButton);
  body.append(continueButton);
}
addButtons();

function stopRolling() {
  clearInterval(timerId);
  isRunning = false;
}
function continueRolling() {
  if (!isRunning) {
    timerId = setInterval(showImage, 1000);
    isRunning = true;
  }
}

stopButton.addEventListener("click", stopRolling);
continueButton.addEventListener("click", continueRolling);
